//UNIX Operator development version, simple menu-based interface for UNIX systems.
//Copyright (C) 2014  Alex Martin

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LIB_MENU_H
#define LIB_MENU_H

#include <stddef.h>
#include <stdbool.h>

typedef void (*uopMenuCallback)(void *, int); // First parameter is the menu, second is the index of the option.

typedef enum uopMenuType {
  function = 0,
  submenu = 1,
  toggle = 2,
  end = 3,
} uopMenuType;

typedef struct uopMenuFunction {
  uopMenuCallback callback;
} uopMenuFunction;

typedef struct uopMenuSubmenu {
  void *menu; // Actually a uopMenu but we can't make a circular typedef.
} uopMenuSubmenu;

typedef struct uopMenuToggle {
  bool state;
} uopMenuToggle;

typedef struct uopMenuOption {
  char *name;
  struct uopMenuOption *next;
  uopMenuType type;
  union {
    uopMenuFunction function;
    uopMenuSubmenu submenu;
    uopMenuToggle toggle;
  };
} uopMenuOption;

typedef struct uopMenu {
  char *title;
  char *text;
  uopMenuOption *options;
} uopMenu;

int addOption(uopMenu *menu, uopMenuOption *option);
uopMenuOption *getOptionWithIndex(uopMenu *menu, int index);
int destroyMenu(uopMenu *menu, bool recursive);
int showMenu(uopMenu *menu);

#define MENU(var, menuTitle, menuText) \
  uopMenu *var = malloc(sizeof(uopMenu)); \
  var->options = NULL; \
  var->title = #menuTitle; \
  var->text = menuText

#define SUBMENU(var, optName, menuOn, submenuPtr) \
  uopMenuOption *var = malloc(sizeof(uopMenuOption)); \
  var->next = NULL; \
  addOption(menuOn, var); \
  var->name = #optName; \
  var->type = submenu; \
  var->submenu.menu = (void *) submenuPtr

#define FUNCTION(var, funcName, menuOn, callbackPtr) \
  uopMenuOption *var = malloc(sizeof(uopMenuOption)); \
  var->next = NULL; \
  addOption(menuOn, var); \
  var->name = #funcName; \
  var->type = function; \
  var->function.callback = callbackPtr

#define TOGGLE(var, tglName, menuOn) \
  uopMenuOption *var = malloc(sizeof(uopMenuOption)); \
  var->next = NULL; \
  addOption(menuOn, var); \
  var->name = #tglName; \
  var->type = toggle; \
  var->toggle.state = false

#define END(var, endName, menuOn) \
  uopMenuOption *var = malloc(sizeof(uopMenuOption)); \
  var->next = NULL; \
  addOption(menuOn, var); \
  var->name = #endName; \
  var->type = end

#define MENU_DYN(var, menuTitle, menuText) \
  uopMenu *var = malloc(sizeof(uopMenu)); \
  var->options = NULL; \
  var->title = menuTitle; \
  var->text = menuText

#define SUBMENU_DYN(var, optName, menuOn, submenuPtr) \
  uopMenuOption *var = malloc(sizeof(uopMenuOption)); \
  var->next = NULL; \
  addOption(menuOn, var); \
  var->name = optName; \
  var->type = submenu; \
  var->submenu.menu = (void *) submenuPtr

#define FUNCTION_DYN(var, funcName, menuOn, callbackPtr) \
  uopMenuOption *var = malloc(sizeof(uopMenuOption)); \
  var->next = NULL; \
  addOption(menuOn, var); \
  var->name = funcName; \
  var->type = function; \
  var->function.callback = callbackPtr

#define TOGGLE_DYN(var, tglName, menuOn) \
  uopMenuOption *var = malloc(sizeof(uopMenuOption)); \
  var->next = NULL; \
  addOption(menuOn, var); \
  var->name = tglName; \
  var->type = toggle; \
  var->toggle.state = false

#define END_DYN(var, endName, menuOn) \
  uopMenuOption *var = malloc(sizeof(uopMenuOption)); \
  var->next = NULL; \
  addOption(menuOn, var); \
  var->name = endName; \
  var->type = end

#endif
