//UNIX Operator development version, simple menu-based interface for UNIX systems.
//Copyright (C) 2014  Alex Martin

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include "menu.h"
#include "util.h"

// Returns the index of the option.
int addOption(uopMenu *menu, uopMenuOption *option) {
  int i = 1;

  if (menu->options == NULL) {
    menu->options = option;
    return 1;
  }

  for (uopMenuOption *current = menu->options; current != NULL; i++) {
    if (current->next == NULL) {
      current->next = option;
      return i;
    }
    current = current->next;
  }
  return -1;
}

uopMenuOption *getOptionWithIndex(uopMenu *menu, int index) {
  int i = 1;

  for (uopMenuOption *current = menu->options; current != NULL; i++) {
    if (i == index) {
      return current;
    }
    current = current->next;
  }

  return NULL;
}

int destroyMenu(uopMenu *menu, bool recursive) {
  for (uopMenuOption *option = menu->options; option != NULL;) {
    if (recursive && option->type == submenu) {
      destroyMenu(option->submenu.menu, true);
    }
    uopMenuOption *next = option->next;
    free(option);
    option = next;
  }
  free(menu);
  return 0;
}

/*
Return values:
0 - User picked a normal option, option was handled.
1 - User picked an `end` option.
-1 - User picked an invalid option.
-2 - Invalid menu.
 */
int showMenu(uopMenu *menu) {
  int selection;
  uopMenuOption *option;
  int numOpts = 1;

  clear();
  puts(menu->text);
  puts("---");
  puts(menu->title);

  for (option = menu->options; option != NULL; numOpts++) {
    switch (option->type) {
    case function:
      printf("%i. %s\n", numOpts, option->name);
      break;
    case submenu:
      printf("%i. %s >\n", numOpts, option->name);
      break;
    case toggle:
      if (option->toggle.state) {
        printf("%i. %s [*]\n", numOpts, option->name);
      } else {
        printf("%i. %s [ ]\n", numOpts, option->name);
      }
      break;
    case end:
      printf("%i. < %s\n", numOpts, option->name);
      break;
    default:
      printf("%i. %s\n", numOpts, option->name);
    }
    option = option->next;
  }

  numOpts--;

  printf("Make a selection[1-%i]: ", numOpts);
  scanf("%i", &selection);
  option = getOptionWithIndex(menu, selection);

  if (option == NULL) {
    return -1;
  }

  switch (option->type) {
  case function:
    option->function.callback(menu, selection);
    break;
  case submenu:
    showMenu(option->submenu.menu);
    break;
  case toggle:
    option->toggle.state = !option->toggle.state;
    break;
  case end:
    return 1;
  default:
    return -2;
  }

  return 0;
}
