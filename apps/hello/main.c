#include <stdio.h>
#include <stdlib.h>
#include "../../lib/util.h"
#include "../../lib/menu.h"

void sayHello(void *menu, int index) {
  puts("Hello, world!\n");
  util_continue();
}

uopMenu *initMenu() {
  char *text = "";
  MENU(menu, App: hello, text);

    FUNCTION(hello, Say Hello, menu, &sayHello);

    END(exitOpt, Exit, menu);

  return menu;
}

int main(int argc, char *argv[]) {
  uopMenu *menu = initMenu();
  int retVal = 0;
  while(retVal == 0) {
    retVal = showMenu(menu);
  }
  destroyMenu(menu, true);
}
