//UNIX Operator development version, simple menu-based interface for UNIX systems.
//Copyright (C) 2014  Alex Martin

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <dirent.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "lib/util.h"
#include "lib/menu.h"

void apps_opt_cmdline(void *menu, int optNum) {
  char *cmd = malloc(128);
  cmd[0] = '\0';
  printf("Enter a command to execute using the shell: ");
  scanf(" %[^\n]", cmd);
  util_shellCmd((const char *) cmd);
  util_continue();
  free(cmd);
}

void apps_run(void *menu, int index) {
  char *cwd = malloc(256);
  getcwd(cwd, 256); // Get the working directory.
  int pid = fork(); // Fork/exec the application.
  if (pid == 0) {
    char *path = malloc(256);
    path[255] = '\0';
    strncpy(path, cwd, 255);
    strcat(path, "/"); // Acquire the application's absolute path.
    strcat(path, getOptionWithIndex(menu, index)->name);
    printf("Running %s...", path);
    execl(path, path, (void *) NULL); // Exec to the application.
    perror("execl"); // If we get this far, there's been an error in execl().
    exit(1);
  } else if (pid == -1) {
    perror("fork");
  } else {
    int status;
    wait(&status);
  }
}

void apps_opt_uop(void *menu, int index) {
  char *dirPath = malloc(256);
  DIR *dir;
  char *blank = "";
  MENU(appMenu, Applications, blank); // Create a menu.
  getcwd(dirPath, 256); // Get the working directory.
  dir = opendir(dirPath);
  if (dir == NULL) {
    if (errno == ENOENT) {
      printf("No such file or directory: %s\n", dirPath);
    } else {
      perror("");
    }
    return;
  }
  while (true) {
    struct dirent *item = readdir(dir);
    char *itemPath = malloc(256);
    itemPath[255] = '\0';
    struct stat *statbuf = malloc(sizeof(struct stat));
    if (item == NULL) {
      break;
    }
    strncpy(itemPath, dirPath, 255);
    strcat(itemPath, "/");
    strcat(itemPath, item->d_name);
    stat(itemPath, statbuf);
    if (S_ISREG(statbuf->st_mode) && (statbuf->st_mode & S_IXUSR || statbuf->st_mode & S_IXGRP || statbuf->st_mode & S_IXOTH)) { // Check that a) anyone has execute permission and b) the file is a regular file.
      FUNCTION_DYN(option, item->d_name, appMenu, apps_run);
    }
  }
  closedir(dir);
  END(back, Back, appMenu);
  showMenu(appMenu);
  destroyMenu(appMenu, true);
}

void about_opt_gpl(void *menu, int optNum) {
  util_viewFile("LICENSE");
}

void about_opt_credits(void *menu, int optNum) {
  util_viewFile("credits.txt");
}

void mainMenu_opt_help(void *menu, int optNum) {
  util_viewFile("help.txt");
}

uopMenu *initMenu() {
  char *text = "";
  MENU(menu, Main menu, text);

    FUNCTION(help, Help, menu, &mainMenu_opt_help);

    text = "Version 0.4-dev by Alex Martin and GitHub contributors.\nCopyright (C) 2014 Alex Martin.\nThis program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.\nUNIX Operator\nVersion 0.4-dev by Alex Martin and GitHub contributors\nCopyright (C) 2014 Alex Martin.";
    MENU(about, About UNIX Operator, text);

    SUBMENU(aboutOpt, About, menu, about);

      FUNCTION(about_gpl, GPLv3, about, &about_opt_gpl);

      FUNCTION(about_credits, Credits, about, &about_opt_credits);

      END(about_back, Back, about);

    text = "";
    MENU(apps, Applications, text);

    SUBMENU(appsOpt, Applications, menu, apps);

      FUNCTION(apps_uop, UNIX Operator applications, apps, &apps_opt_uop);

      FUNCTION(apps_cmdline, Command-line applications, apps, &apps_opt_cmdline);

      END(apps_back, Back, apps);

    END(exitOpt, Exit, menu);

  return menu;
}

int main(int argc, char *argv[]) {
  fprintf(stderr, "%s: Warning: The C version of UNIX Operator is not ready to be used.\n", argv[0]);
  uopMenu *menu = initMenu();
  int retValue = 0;
  while (retValue == 0) {
    retValue = showMenu(menu);
  }
  destroyMenu(menu, true);
  return 0;
}
