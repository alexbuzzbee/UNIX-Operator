UNAME_S = $(shell uname -s)

ifndef CC
	CC = gcc
endif
CCFLAGS = -std=gnu99 -fpic # -fpic set for reasons of library.
LD = ${CC} # Using compiler as linker, for reasons of failed local builds.
LDFLAGS = -L${PWD} # -L${PWD} makes libuop get found.

ifeq (${UNAME_S}, Darwin)
	SLIBEXT = dylib
else
	SLIBEXT = so
endif

all: libuop uop hello

test: uop
	bash test.sh

clean: clean-obj clean-bin

clean-obj:
	rm *.o apps/hello/*.o lib/*.o

clean-bin:
	rm uop libuop.${SLIBEXT} hello

uop: main.o
	${LD} ${LDFLAGS} main.o -luop -o uop

hello: apps/hello/main.o
	${LD} ${LDFLAGS} apps/hello/main.o -luop -o hello

libuop: libuop.${SLIBEXT}

libuop.so: lib/util.o lib/menu.o
	${LD} ${LDFLAGS} lib/util.o lib/menu.o -shared -o libuop.so

libuop.dylib: lib/util.o lib/menu.o
	${LD} ${LDFLAGS} lib/util.o lib/menu.o -shared -o libuop.dylib

main.o: main.c lib/util.h lib/menu.h
	${CC} ${CCFLAGS} -c main.c -o main.o

apps/hello/main.o: apps/hello/main.c lib/util.h lib/menu.h
	${CC} ${CCFLAGS} -c apps/hello/main.c -o apps/hello/main.o

lib/util.o: lib/util.c lib/util.h
	${CC} ${CCFLAGS} -c lib/util.c -o lib/util.o

lib/menu.o: lib/menu.c lib/menu.h
	${CC} ${CCFLAGS} -c lib/menu.c -o lib/menu.o
